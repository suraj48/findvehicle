import 'package:flutter/material.dart';
import 'package:parkingHelper/findvehicle.dart';
import 'package:parkingHelper/services/functions.dart';
import 'package:parkingHelper/shared/loading.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            appBar: AppBar(
              title: Center(child: Text("Parking Helper", style: TextStyle())),
              backgroundColor: Colors.grey[900],
            ),
            body: Container(
              color: Color(0xff373a40),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RaisedButton(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                        color: Color(0xff19d3da),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.add_location, size: 36),
                            SizedBox(
                              width: 14,
                            ),
                            Text(
                              'mark current location',
                              style: TextStyle(fontSize: 19),
                            )
                          ],
                        ),
                        onPressed: () {
                          showAlertDialog(context);
                        }),
                    SizedBox(
                      height: 75,
                    ),
                    RaisedButton(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                        color: Color(0xff19d3da),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.my_location, size: 36),
                            SizedBox(
                              width: 14,
                            ),
                            Text('find my vehicle',
                                style: TextStyle(fontSize: 19))
                          ],
                        ),
                        onPressed: () async {
                          setState(() {
                            loading = true;
                          });
                          //check if you saved coordinates...
                          bool exists = await Functions().ifCoordinatesExists();
                          if (exists) {
                            Functions().grabCoordinates();
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => FindVehicle()));
                          }
                          setState(() {
                            loading = false;
                          });
                        }),
                  ],
                ),
              ),
            ),
          );
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(
        "Cancel",
        style: TextStyle(color: Color(0xff19d3da)),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(
        "Continue",
        style: TextStyle(color: Color(0xff19d3da)),
      ),
      onPressed: () async {
        Navigator.pop(context);
        setState(() {
          loading = true;
        });
        bool success = await Functions().pushCoordinates();
        await Future.delayed(Duration(milliseconds: 1500));
        if (success) {
          Fluttertoast.showToast(msg: "successfully saved location");
        } else {
          Fluttertoast.showToast(msg: "failed...");
        }
        setState(() {
          loading = false;
        });
      },
    ); // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      backgroundColor: Colors.grey[800],
      title: Text(
        "Warning",
        style: TextStyle(color: Colors.white),
      ),
      content: Text(
        "Previously entered location\nwill be overridden!",
        style: TextStyle(color: Colors.white),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    ); // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
