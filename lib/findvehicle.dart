import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:parkingHelper/shared/loading.dart';
import 'dart:async';
import 'shared/globals.dart' as globals;
import 'package:location/location.dart';

class FindVehicle extends StatefulWidget {
  @override
  _FindVehicleState createState() => _FindVehicleState();
}

class _FindVehicleState extends State<FindVehicle> {
  Uint8List imageData;
  GoogleMapController _controller;
  BitmapDescriptor sourceIcon;
  Location location = Location();
  LocationData myLocationData;
  bool loading = true;
  int distance = 0;

  List<Marker> allMarkers = [];

  setDistance() {
    double distanceInMeters = Geolocator.distanceBetween(
        myLocationData.latitude,
        myLocationData.longitude,
        globals.vehicleCoordinates[0],
        globals.vehicleCoordinates[1]);
    distance = distanceInMeters.toInt();
  }

//grabbing arrow png file from asset, and putting it on gmaps as bytedata
  setIcon() async {
    ByteData byteData =
        await DefaultAssetBundle.of(context).load("assets/images/arrow.png");
    imageData = byteData.buffer.asUint8List();

    sourceIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5), 'assets/images/arrow.png');
  }

  moveCameraToMyLocation() {
    _controller.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(
        zoom: 17,
        target: LatLng(myLocationData.latitude, myLocationData.longitude))));
  }

  updateMyLocationMarker() async {
    allMarkers[1] = Marker(
      markerId: MarkerId('myLocation'),
      rotation: myLocationData.heading,
      draggable: false,
      flat: true,
      icon: BitmapDescriptor.fromBytes(imageData),
      position: LatLng(myLocationData.latitude, myLocationData.longitude),
    );
  }

  void initState() {
    super.initState();

    print(globals.vehicleCoordinates[1]);
    allMarkers.add(Marker(
      infoWindow: InfoWindow(title: 'your vehicle'),
      markerId: MarkerId('vehicle'),
      draggable: false,
      position:
          LatLng(globals.vehicleCoordinates[0], globals.vehicleCoordinates[1]),
    ));
    allMarkers.add(Marker(
      markerId: MarkerId('myLocation'),
      draggable: false,
      position:
          LatLng(globals.vehicleCoordinates[0], globals.vehicleCoordinates[1]),
    ));

    //listening to location stream...
    location.onLocationChanged.listen((value) async {
      await setIcon();
      setState(() {
        myLocationData = value;
        updateMyLocationMarker();
        setDistance();
      });
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            appBar: AppBar(
              title: Text("your Vehicle's location"),
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Stack(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height - 170,
                        width: MediaQuery.of(context).size.width,
                        child: GoogleMap(
                          initialCameraPosition: CameraPosition(
                              target: LatLng(myLocationData.latitude,
                                  myLocationData.longitude),
                              zoom: 18),
                          markers: Set.from(allMarkers),
                          onMapCreated: (GoogleMapController controller) {
                            _controller = controller;
                          },
                        ),
                      ),
                      Positioned(
                        top: 20,
                        right: 10,
                        child: FloatingActionButton(
                          backgroundColor: Colors.blue,
                          onPressed: () {
                            moveCameraToMyLocation();
                          },
                          child: Icon(Icons.my_location),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Text('you are $distance meters away',
                      style: TextStyle(fontSize: 19)),
                ],
              ),
            ),
          );
  }
}
