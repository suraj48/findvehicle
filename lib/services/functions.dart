import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:parkingHelper/shared/globals.dart' as globals;

class Functions {
  pushCoordinates() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      prefs.setDouble('latitude', position.latitude);
      prefs.setDouble('longitude', position.longitude);
      print('ok?');
    } catch (e) {
      return false;
    }
    return true;
  }

  ifCoordinatesExists() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('latitude')) {
      return true;
    }

    return false;
  }

  grabCoordinates() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    globals.vehicleCoordinates[0] = prefs.getDouble('latitude');
    globals.vehicleCoordinates[1] = prefs.getDouble('longitude');
  }

  // Stream<Position> get position(){
  //   StreamSubscription as;
  // }

}
